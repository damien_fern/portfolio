export const defaultLocale = 'en'

export const locales = [
    {
        code: 'en',
        name: 'English',
        dir: 'ltr',
    },
    {
        code: 'fr',
        name: 'Français',
        dir: 'ltr',
    }
]